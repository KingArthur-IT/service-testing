function testService(events) {
    let personsInShopObject = {};

    for (const event of events) {
        if (!event && event.length !== 2) return false

        if (event[1] === 'in' && !personsInShopObject.hasOwnProperty(event[0]))
            personsInShopObject[event[0]] = 'in';
        else if (event[1] === 'out' && personsInShopObject.hasOwnProperty(event[0]))
                delete personsInShopObject[event[0]]
             else return false
    }

    return Object.keys(personsInShopObject).length === 0
};

module.exports = testService
